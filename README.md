# nodeJSmyApp

## Getting Started
### You'll need to install Node.Js (https://nodejs.org/en/) as well some of the modules used in this app. 

```
 npm install {nameOfTheModule}

- express = require('express');
- app = express();
- http = require('http');
- Cookies = require("cookies")
- cookieParser = require('cookie-parser')
- bodyParser = require('body-parser')
- dns = require('dns');
```
Possibly more should give you an error in the cmd if more are needed: 

## Running the App
- Navigate to directory in cmd  
- use command 'node app.js'
- Open browser at http://localhost:3000/

## Authors
Timothy Field @ LYONSCG


