﻿var express = require('express');
var app = express();
var http = require('http');
var Cookies = require("cookies")
var cookieParser = require('cookie-parser')

app.set('port', process.env.PORT || 3000);

app.use(express.static('public'))
app.use(cookieParser());
app.use(require('./routes/index'));
app.use(require('./routes/cookieEditor'));
 
var server = app.listen(app.get('port'), function () {
    console.log('Listening on port ' + app.get('port') + '!');
});
