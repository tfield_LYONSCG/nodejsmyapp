﻿var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser')
var postInfo = [];
var cookieName = "";
var cookieValue = "";
router.use(bodyParser.urlencoded({ extended: true })); 


router.post('/cookieEditor', function (req, res) {
var cookieName =  req.body.name;
var cookieValue = req.body.cookValue;
    res.set('Content-Type', 'text/html')
    var cookie = req.cookies.cookieName;
    res.cookie(cookieName, cookieValue, { maxAge: 900000, httpOnly: true });
    console.log(cookieName + 'with ' + cookieValue +' Created Successfully');
    res.redirect('/');
    res.end();
});

router.get("/cookieEditor", function (req, res, next) {
    res.write(`
    <head>

        <style> 
        * {text-align: center;margin:1em;   background-image: url("images/darkPat4.png"); color:white;} 
                table { margin:1em auto;font-family: arial, sans-serif; border-collapse: collapse; width: 75%;}
            td, th { border: 2px solid black; text-align: left; padding: 8px;}
            tr:nth-child(even) {   
            background-color: #dddddd;}
            div.content {font-size: 15px;}
        </style> 
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300" rel="stylesheet">

            <link rel = "stylesheet" href = "css/style.css" />

    </head> `);
   
    res.write(`   
    <body> 
        <div class="header">
             <h3>  <button style="float:left" onclick="window.location.href='http://localhost:3000'" class="up" >Go back home </button>   
             </h3>
             <br><br>
   <h1> Welcome to the Cookie Editor Page </h1>
        <hr>
        </div>

        <div class="content">
            <img   src = "images/sponge.gif" width="350px" height="370px" align="left" >   
            <img src = "images/sponge.gif" width="350px" height="370px" align="right" >     
            <form method="post">   <h2>Hello there! <BR> Enter your Cookie Name and Cookie Value below and we will generate a delicious cookie! </h2><BR>
             <BR>Cookie Name:* 
                 <input type="text" id="name" name="name" placeholder="Enter Your Cookie Name" required="true" /> <br>
                 Cookie Value:*
                  <input type="text" id="cookValue" name="cookValue" placeholder="Enter Your Cookie Value" required="true" />  <br>
                  <h2> <button class="up">Make that cookie </button>    </h2>
            </form> 
        </div><br><br>
        <hr>
    </body>
    `);
    res.end(); 
});

module.exports = router;