﻿var express = require('express');
var router = express.Router();
var dns = require('dns');
var ip = '';
var qs = require('querystring');
const bodyParser = require('body-parser')
var postInfo = [];

router.get("/", function (req, res, next) {

    var cookie = req.cookies.cookieName;
    if (cookie === undefined) {
        var randNumb = Math.random().toString();
        randNumb = randNumb.substring(2, randNumb.length);
        res.cookie(('MyRandomCookie'), randNumb, { maxAge: 9000, httpOnly: true });
        console.log('MyRandomCookie Created Successfully');
    }
    else {
        console.log('cookie exists', cookie);
    }
   next(); 

    dns.lookup(require('os').hostname(), function (err, add, fam) {
        ip = add;
    })
    res.write(`  
    <head>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300" rel="stylesheet">
    <link rel = "stylesheet" href = "css/style.css" />
    <style>
    table {  
        background-image: url("images/darkPat.png");
    }
    tr:nth-child(even) {background-image: url("images/darkPat_.png");
}
   </style>
    </head>   
   

<body>
    <div class= "header">
        <div id="right">  
            <h2 style="color:white; font-family: Fira Sans;"> Powered by Node.js </h2> 
            <img src="https://nodejs.org/static/images/logos/nodejs-new-pantone-black.png" width="150px" height="100px" > 
        </div>
        <br> <br> <br>
       
        <div id="left">
            <h3> <button style="float:left" onclick="window.location.href='http://localhost:3000/cookieEditor'" class="up" >Go to Cookie Editor </button> </h3>    
        </div>

        <div id="center"> 
        <h1> Welcome To The Home Page   </h1>
        </div>
    </div>
        <hr>  
              <div class="content">  
              <table>
              <tr  class="up"><th>Attribute</th><th>Value</th></tr>
              <tr class="up"> <td> Client IP: </td><td> "` + ip + `" </td></tr>
              <tr class="up"> <td> Referrer </td><td> " `+ req.header('Referer') +`" </td></tr>
              <tr class="up"> <td> All cookies and values </td><td>  "` + JSON.stringify(req.cookies) + `" </td></tr>
              <tr class="up"> <td> Post Info </td><td> "` +      postInfo + `" </td></tr>
              </table>
              </div>     
             
              <div class="footer">
              <hr>  
              <form method="post" onsubmit="val(this)">
              <h3>Enter what you want to post to the server:</h3>
              <input type="text" id="name" name="name" placeholder="Enter Your Text!" required=true; />       
                 <h3>   <button  class="up">Submit</button> </h3>
                 <br>  <br> 
              </form>  
            <script>
            function val(x){
                var t = document.getElementById("name");
                var strEntered = t.value;
              
                // alert(strEntered);
                 if (strEntered.includes("<")){
                    t.value="not a valid entry";
                 }
            }
            </script>
            <hr>
              </div>
              <div id="lower_footer" class="clearfix">
              <BR> 
            </div>
              </body>
    `);
    res.end(); 
});

router.use(bodyParser.urlencoded({ extended: true })); 

router.post('/', function (req, res) {
    postInfo.push(req.body.name);
      res.set('Content-Type', 'text/html')
    res.redirect('/');
    })

module.exports = router;

